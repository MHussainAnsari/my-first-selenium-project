@echo off


echo[
@echo++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@echo Run Maven test
@echo++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
call mvn test
echo[
echo[


call taskkill /f /im chromedriver.exe
call taskkill /f /im firefoxdriver.exe
call taskkill /f /im edgedriver.exe
call taskkill /f /im InternetExplorerDriver.exe
@echo++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@pause

