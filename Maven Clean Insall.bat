@echo off


echo[
@echo++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@echo Maven Clean Install
@echo++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
call mvn clean install
echo[
echo[




call taskkill /f /im chromedriver.exe
call taskkill /f /im firefoxdriver.exe
call taskkill /f /im edgedriver.exe
call taskkill /f /im InternetExplorerDriver.exe
@echo++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
@pause



